#include <iostream>
#include <fstream>
using namespace std;

struct siatka;
struct element;
struct wezel;

struct siatka {
    int lw;         // liczba wezlow
    wezel *w;       // tablica wezlow
    int le;         // liczba elementow
    element *e;     // tablica elementow

    siatka(int nw, wezel *w, int ne, element *e) : lw(nw), w(w), le(ne), e(e) {}
    siatka() {}
};

struct element {
    int id_1;    // poczatkowy wezel
    int id_2;    // koncowy wezel
    double s;          // pole powierzchni
    double k;          // stala
    double l;          // dlugosc elementu
    double **h;        // macierz C
    double *p;         // dodany wektor qS

    element(int id_1, int id_2, double s, double k, double l, double **h, double *p)
            : id_1(id_1), id_2(id_2), s(s), k(k), l(l), h(h), p(p) {}
    element() {}
};

struct wezel {
    int id;     // id wezla
    double x;      // polozenie wezla
    double bc;     // czy posiada boundary condition
                // -1 - konwekcja, 0 - brak, 1 - strumien

    wezel(int id, double x, double bc) : id(id), x(x), bc(bc) {}
    wezel() {}
};


void print(double **tablica, double *wyniki, int wymiar);

void gauss(int wymiar, double **tablica, double *b, bool debug) {
    double **L = new double *[wymiar];
    double **U = new double *[wymiar];
    double *wyniki = new double[wymiar];

    for (int i = 0; i < wymiar + 1; i++) {
        L[i] = new double[wymiar];
        U[i] = new double[wymiar];
    }
    for (int i = 0; i < wymiar; i++) {
        for (int j = 0; j < wymiar ; j++) {
            U[i][j] = 0;
            L[i][j] = 0;
        }
    }
    if (debug) {
        cout << "\nOtrzymano macierz:" << endl;
        print(tablica, b, wymiar); //pokazujemy poczatkowa macierz
    }

    double sum = 0;
    double sum2 = 0;
    for (int i = 0; i < wymiar; i++) {
        for (int j = 0; j < wymiar; j++) {
            for (int k = 0; k <= i - 1; k++) {
                sum = sum + (L[i][k] * U[k][j]);
                sum2 = sum2 + (L[j][k] * U[k][i]);
            }
            if (i < j) {
                U[i][j] = (tablica[i][j] - sum);
                L[j][i] = (tablica[j][i] - sum2) / U[i][i];
            }
            else if (i == j) {
                U[i][j] = (tablica[i][j] - sum);
                L[j][i] = 1;
            }
            else {
                U[i][j] = 0;
                L[j][i] = 0;
            }
            sum = 0;
            sum2 = 0;
        }
    }
    double *y = new double[wymiar];
    double *x = new double[wymiar];
    for (int i = 0; i < wymiar; i++) {
        x[i] = 0;
        y[i] = 0;
    }
    for (int i = 0; i < wymiar; i++){
        double sum_y = b[i];
        for (int j = i - 1; j >= 0; j--){
            sum_y -= L[i][j] * y[j];
        }
        y[i] = sum_y / L[i][i];
    }

    for (int i = wymiar - 1; i >= 0; i--){
        double sum_x = y[i];
        for (int j = wymiar - 1; j > i; j--){
            sum_x -= U[i][j] * x[j];
        }
        x[i] = sum_x / U[i][i];
    }


    cout << "Temperatury: [";
    for (int i = 0; i < wymiar; i++){
        cout << " " << -x[i];
    }
    cout << " ]\n";
}

void print(double **tablica, double *wyniki, int wymiar) {
    for (int i = 0; i < wymiar; i++) {
        for (int j = 0; j < wymiar; j++) {
            cout <<right<< tablica[i][j] << "  ";
        }
        cout << endl;
    }
    cout << endl << "Oraz:" << endl;
    for (int i = 0; i < wymiar; i++) {
        cout <<right << " "<< wyniki[i] <<endl;
    }
    cout << endl;
}


void generujSiatke() {
    fstream plik;
    plik.open("dane.txt");

    if (!plik.is_open()) {
        cout << "Couldn't read from file\n";
    }
    // liczba elementow, liczba wezlow
    int le, lw;
    double bc;
    double L, x;
    bool debug;
    plik >> debug;
    if (debug) {
        cout << "Uruchomiono wersje debug- wyswietlone zostana wszystkie obliczenia" << endl;
    }

    double q, S, alfa, talfa;
    plik >> q >> S >> alfa >> talfa;

    plik >> le >> lw;
    plik >> L;
    x = L / le;

    // Tworzenie wezlow
    wezel *w = new wezel[lw];
    for(int i = 0; i < lw; i++) {

        plik >> x >> bc;
        w[i] = wezel(i, x, bc);
    }
    if (debug) {
        // Sprawdzenie wartosci wezlow
        for (int i = 0; i < lw; i++){
            cout << "Wezel nr. " << w[i].id + 1 << " posiada x = " << w[i].x << " oraz bc = " << w[i].bc << endl;
        }
        cout << endl;
    }
    //Wygenerowanie elementow na podstawie wezlow
    element *e = new element[le];
    for (int i = 0; i < le; i++){
        int id_1, id_2;
        double s, k, l;
        double **h = new double *[le];
        double *p = new double[le];
        for(int i = 0; i < 2; i++){
            h[i] = new double[le];
        }
        // Macierz C
        plik >> id_1 >> id_2 >> s >> k;
        // Dlugosc elementu ustalana na podstawie polozenia dwoch wezlow
        l = w[id_2].x - w[id_1].x;
        double C = (s * k) / l;

        h[0][0] = C;
        h[0][1] = -C;
        h[1][0] = -C;
        h[1][1] = C;
        if (debug) {
            cout << "Macierz C: " << w[i].id + 1<< endl;
            for(int i = 0; i < 2; i++){
                for(int j = 0; j < 2; j++){
                    cout << h[i][j] << ' ';
                }
                cout << endl;
            }
            cout << endl;
        }
        // Macierz p
        if (i == 0){
            p[0] = q * S;
            p[1] = 0;
        } else if (i == le - 1){
            p[0] = 0;
            p[1] = -alfa*talfa*S;
        } else {
            p[0] = 0;
            p[1] = 0;
        }
        if (debug) {
            cout << "Macierz p dla elementu: " << w[i].id + 1<< endl;
            for (int op = 0 ; op < 2; op++){
                cout << p[op] << endl;
            }
            cout << endl;

        }
        e[i] = element(w[i].id, w[i+1].id, s, k, l, h, p);
    }
    if (debug) {
        // Sprawdzenie wartosci wezlow
        for (int i = 0; i < le; i++){
            cout << "Element nr. " << e[i].id_1 + 1 << " - " << e[i].id_2 << " posiada s = " << e[i].s << " oraz k = " << e[i].k << " oraz l = " << e[i].l << endl;
        }
        cout << endl;
    }
    //Sumowanie macierzy P i H do macierzy globalnych;
    cout << "Ilosc wezlow: " << lw << " , a ilosc elementow: " << le << endl;
    double **GH = new double *[lw];
    for (int i = 0; i < lw; i++){
        GH[i] = new double[lw];
    }

    for (int i = 0; i < lw; i++){
        for (int j = 0; j < lw; j++){
            GH[i][j] = 0;
        }
    }


    for (int i = 0; i < le; i++){
        GH[i][i] += e[i].h[0][0];
        GH[i][i+1] += e[i].h[0][1];
        GH[i+1][i] += e[i].h[1][0];
        GH[i+1][i+1] += e[i].h[1][1];
    }
    GH[le][le] += alfa * S;

    if (debug) {
        cout << "Macierz globalna GH" << endl;
        for (int z = 0; z < lw; z++) {
            for (int x = 0; x < lw; x++){
                cout << GH[z][x] << "\t";
            }
            cout << endl;
        }
        cout << endl;
    }
    double *GP = new double[lw];
    GP[0] = e[0].p[0];
    for (int i = 1; i < lw; i++) {
        GP[i] = e[i-1].p[1] + e[i-1].p[0];
        if(i == 1){
            GP[i] = e[i-1].p[1] + e[i].p[0];
        }
    }
    /*
    GP[0] = e[0].p[0];
    GP[1] = e[0].p[1] + e[1].p[0];
    GP[2] = e[1].p[1] + e[2].p[0];
    GP[3] = e[2].p[1] + e[2].p[0];
     */
    if (debug) {
        cout << "Macierz globalna PH" << endl;
        for (int i = 0; i < lw; i++){
            cout << GP[i] << endl;
        }
        cout << endl;
    }
    plik.close();

    gauss(lw, GH, GP, debug);
}





int main() {
    generujSiatke();
    return 0;
}